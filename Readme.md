# Daliance Project #

**Description:**

A gateway application to ETL (Extract-Transform-Load) *any data, any files, anything* to be searchable. Exploiting ApacheTika extraction features and the flexibility of ElasticSearch to handle fluid data structure, backed with HBase data store to stow metadata and original binary data. In hope to distinguish between documents, we *shall* employ simple K-Nearest Neighbor with Jaccard Index upon metadata fields.

**Libraries:**

* [Netbeans 8.1](https://netbeans.org/downloads/)
* [Java Development Kit 6+](www.oracle.com/technetwork/java/javase/downloads/)
* [Apache HTTP Client 4.5.1](https://hc.apache.org/downloads.cgi)
* [Google JSON 2.5](https://github.com/google/gson)
* [Tomcat Server Registered in Netbeans](https://netbeans.org/kb/index.html) you can use [XAMPP bundled tomcat](https://www.apachefriends.org/index.html) for less configuration requirement.

**Application Stack:**

All application below should be configured correctly or this `gateway` won't work at all. HBase and ElasticSearch can work in Standalone Mode which require less hassle and trouble.

* [Apache Tika 1.11](http://tika.apache.org/download.html)
* [HBase 1.2.1](http://www.eu.apache.org/dist/hbase/1.1.2/) - **unimplemented**
* [ElasticSearch 2.1](https://www.elastic.co/downloads)

**Configuration:**

All configuration stored in `org.gamabox.daliance.config/configuration.xml` - please configure `configuration-example.xml` file accordingly and save it as `configuration.xml`.

**Technical Note:**

HTTP Routes:

* **GET**    `/Daliance/` hello world.

* **GET**    `/Daliance/Indexer` exposes all indexer url below.

* **GET**    `/Daliance/Indexer/Item` get all item.
* **POST**   `/Daliance/Indexer/Item` insert a new item into server.

* **GET**    `/Daliance/Indexer/Type` get all item types.
* **GET**    `/Daliance/Indexer/Type/{TypeName}` get all items within given item type.

* **GET**    `/Daliance/Indexer/Type/{TypeName}/Item` get all item within given item type.
* **GET**    `/Daliance/Indexer/Type/{TypeName}/Item/{UUID}` get a item for given UUID, returns 404 if given UUID did not match given TypeName.
* **GET**    `/Daliance/Indexer/Type/{TypeName}/Item/{UUID}/Binary` get a item binary data for given UUID, returns 404 if given UUID did not match given TypeName.
* **PUT**    `/Daliance/Indexer/Type/{TypeName}/Item/{UUID}` update item for given UUID, returns 404 if given UUID did not match given TypeName.
* **DELETE** `/Daliance/Indexer/Type/{TypeName}/Item/{UUID}` delete item for given UUID, returns 404 if given UUID did not match given TypeName.

* **GET**    `/Daliance/Trainer` exposes all trainer url below.

* **GET**    `/Daliance/Trainer/Type` get list of all item type.
* **GET**    `/Daliance/Trainer/Type/{TypeName}` get all training item for given item type.
* **GET**    `/Daliance/Trainer/Type/{TypeName}/Item` get all training item within given item type.
* **GET**    `/Daliance/Trainer/Type/{TypeName}/Item/{UUID}` get a new training item for given UUID, returns 404 if given UUID did not match given TypeName.
* **POST**   `/Daliance/Trainer/Type/{TypeName}/Item` put a training item into given item type
* **PUT**    `/Daliance/Trainer/Type/{TypeName}/Item/{UUID}` update training item for given UUID, returns 404 if given UUID did not match given TypeName.
* **DELETE** `/Daliance/Trainer/Type/{TypeName}/Item/{UUID}` delete training item for given UUID, returns 404 if given UUID did not match given TypeName.

Trainer URL to train the categorizer. Any changes to the training data will render previously inferred document type (mapping) to be invalid, but the application have no way to check this or to do re-indexing (as requests to ElasticSearch can be done concurrently). Such laborious act should be done in maintenance mode. It is advised to *train* the system before being used to index.

* **POST**   `/Daliance/Searcher` search with ElasticSearch, expects JSON-encoded request.
