/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.config;

import java.util.Map;
import java.util.HashMap;

/**
 * Entity for each configuration object
 * @author Tezla
 */
class Configuration implements Comparable<Configuration> {
    private String identifier;
    private final Map<String, String> semantics = new HashMap<String, String>();
    
    public Configuration(String identifier){
        this.identifier = identifier;
    }
    
    public void setIdentifier(String identifier){
        this.identifier = identifier;
    }
    
    public void putConfiguration(String Key, String Object){
        semantics.putIfAbsent(Key, Object);
    }
    
    public String getIdentifier(){
        return this.identifier;
    }
    
    public String getConfiguration(String Key){
        return semantics.getOrDefault(Key, null);
    }
    
    @Override
    public String toString(){
        String out = this.identifier + "\n";
        for(String key : this.semantics.keySet()){
            out += key + " " + this.semantics.get(key) + "\n";
        }
        return out;
    }

    @Override
    public int compareTo(Configuration o) {
        if(o != null)
            return this.identifier.compareTo(o.getIdentifier());
        else
            throw (new NullPointerException());
    }
}
