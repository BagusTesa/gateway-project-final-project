/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.config;
import java.io.InputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

import org.gamabox.daliance.helper.DalianceException;

/**
 * Reader's hook that serve configuration.xml file as stream for reader to read
 * @author Tezla
 */
public class ConfigProvider{
    private static ConfigProvider instance;
    private transient List<Configuration> configs;
    
    private ConfigProvider() throws DalianceException{
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try{
            InputStream xmlinput = getClass().getResourceAsStream("configuration.xml");
            SAXParser parser = factory.newSAXParser();
            Reader handler = new Reader();
            parser.parse(xmlinput, handler);
            configs = handler.configurations;
            Collections.sort(configs);
        }
        catch(SAXException e){
            throw(new DalianceException("SAXException on " + getClass().getName(), e));
        }
        catch(IOException e){
            throw(new DalianceException("IOException on " + getClass().getName(), e));
        }
        catch(ParserConfigurationException e){
            throw(new DalianceException("ParserConfigurationException on " + getClass().getName(), e));
        }
    }
    
    /**
     * Returns instance of ConfigProvider and set it up if required
     * Possible bottleneck, synchronized - better to use eager load singleton
     * @return ConfigProvider
     * @throws DalianceException
     */
    public synchronized static ConfigProvider getInstance() throws DalianceException{
        if(instance == null){
            instance = new ConfigProvider();
        }
        return instance;
    }
    
    
    @Override
    public String toString(){
        String out = "";
        if(!configs.isEmpty()){
            for(Configuration entity : configs){
                out += entity.toString();
                out += "\n";
            }
        }
        return out;
    }
    
    public String getConfiguration(String entityName, String fieldName){
        if(configs != null){
            Configuration arb = new Configuration(entityName);
            int index = Collections.binarySearch(configs, arb);
            if(index > -1){
                return configs.get(index).getConfiguration(fieldName);
            }
        }
        return null;
    }
}
