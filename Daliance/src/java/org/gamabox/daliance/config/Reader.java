/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.config;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * Reads XML from stream and initialize configuration entities
 * @author Tezla
 */
class Reader extends DefaultHandler{
    //stores the real configuration entities
    public final List<Configuration> configurations = new ArrayList<Configuration>();
    //for validation and processing
    private final Stack<String> elementStack = new Stack<String>();
    private final Stack<Configuration> objectStack = new Stack<Configuration>();
    
    private Configuration currentObject(){
        return this.objectStack.peek();
    }
    
    private String currentElement(){
        return this.elementStack.peek();
    }
    
    private String currentElementParent(){
        if(this.elementStack.size() < 2){
            return null;
        }
        else{
            return this.elementStack.get(this.elementStack.size() - 2 );
        }
    }
    
    @Override
    public void startElement(String uri, String localname, String qName, Attributes attributes) throws SAXException{
        this.elementStack.push(qName);
        if((currentElementParent() != null ) && currentElementParent().equals("daliance")){
            //all element below the root is configuration entity
            Configuration nw = new Configuration(qName);
            this.objectStack.push(nw);
            this.configurations.add(nw);
        }
    }
    
    @Override
    public void endElement(String uri, String localname, String qName) throws SAXException{
        if((currentElementParent() != null ) && currentElementParent().equals("daliance")){
            this.objectStack.pop();
        }
        if(!this.elementStack.pop().equals(qName)){
            throw new SAXException("Not well formed");
        }
    }
    
    @Override
    public void characters(char ch[], int start, int length) throws SAXException{
        String value = new String(ch, start, length).trim();
        if((value.length() != 0) && (currentElementParent() != null ) && !(currentElementParent().equals("daliance"))){
            if(currentElement().equals("name")){
                currentObject().setIdentifier(value);
            }
            else{
                currentObject().putConfiguration(currentElement(), value);
            }
            
        }
    }
    
}
