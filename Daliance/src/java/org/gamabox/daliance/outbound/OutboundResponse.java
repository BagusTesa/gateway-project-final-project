/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.outbound;

/**
 * Aggregates Apache HttpComponent response content and HTTP status code.
 *
 * @author Tezla
 */
public class OutboundResponse {
    private final int StatusCode;
    private final String Content;
    
    protected OutboundResponse(int StatusCode, String Content){
        this.StatusCode = StatusCode;
        this.Content = Content;
    }
    
    /**
     * @return the StatusCode
     */
    public int getStatusCode() {
        return StatusCode;
    }

    /**
     * @return the Content
     */
    public String getContent() {
        return Content;
    }
    
}
