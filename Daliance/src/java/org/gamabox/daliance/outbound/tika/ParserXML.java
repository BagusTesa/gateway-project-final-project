/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.tika;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.logic.MetaContainer;
import org.gamabox.daliance.logic.keywords.PostParser;


/**
 *
 * @author Tezla
 */
class ParserXML {
    private class InternalXMLHandler extends DefaultHandler{
        public final Map<String, Object> output = new HashMap<String, Object>();
        private final Stack<String> nodes = new Stack<String>();
        private boolean oldAccumulatorGuard = false;
        private String oldAccumulator;
        private String accumulator;
        
        private String storeIgnoreTag(String qName, Attributes attributes){
            String out = "";
            int num_attr = attributes.getLength();
            out += "<" + qName;
                    if(num_attr > 0){
                        out += " ";
                    }
                    for(int seek = 0; seek < num_attr; seek++){
                        out += attributes.getQName(seek) + "=" + "\"" + attributes.getValue(attributes.getQName(seek)) + "\"";
                        if((seek + 1) < num_attr){
                            out += " ";
                        }
                    }
            out += ">";
            return out;
        }
        
        private void appendListToMap(String key, String value){
            List<String> inlist;
            if(output.containsKey(key) && (output.get(key) instanceof List)){
                //if key existed and instanceof a List
                inlist = (List) output.get(key);
            }
            else{
                //if key doesn't exist initialize a list for it
                inlist = new LinkedList<String>();
                output.put(key, inlist);
            }
            inlist.add(value);
        }
        
        @Override
        public void startElement(String uri, String localname, String qName, Attributes attributes) throws SAXException{
            String key, content;
            if(qName.equalsIgnoreCase("meta") && (attributes.getValue("name") != null)){
                key = attributes.getValue("name");
                if((key != null) && (!key.contains("Unknown"))){
                    content = attributes.getValue("content");
                    if(key.contains("Content-Type")){
                        String tmp[] = attributes.getValue("content").replace(' ', '\0').split(";");
                        if(tmp.length > 1){
                            content = tmp[0];
                        }
                    }
                    output.put(key, content);
                }
            }
            else if(qName.equalsIgnoreCase("div")){
                if((attributes.getValue("class") != null) && (attributes.getValue("class").equalsIgnoreCase("embedded"))){
                    //store embedded id in XML <div class="embedded" id="something"/>
                    key = "embedded";
                    appendListToMap(key, attributes.getValue("id"));
                }
                if((attributes.getValue("class") != null) && (attributes.getValue("class").equalsIgnoreCase("package-entry")) && !oldAccumulatorGuard){
                    //store content in XML <div class="embedded" id="something"/>
                    key = "embedded-content";
                    oldAccumulatorGuard = true;
                    oldAccumulator = accumulator;
                    accumulator = "";
                    nodes.push(key);
                }
                else{
                    //accumulates any tags including <div> tags inside <body>...</body> but discards any <div class="embedded" ../>
                    accumulator += storeIgnoreTag(qName, attributes);
                    nodes.push(qName);
                }
            }
            else if(qName.equalsIgnoreCase("body")){
                //start accumulate <body>..</body> contents
                accumulator = "";
                nodes.push("body");
            }
            else if(qName.equalsIgnoreCase("title")){
                //start accumulate <title>...</title> contents
                accumulator = "";
                nodes.push("title");
            }
            else if((nodes.size() > 0) && (nodes.contains("body"))){
                //accumulates tags inside <body>...</body> but discards any embedded
                accumulator += storeIgnoreTag(qName, attributes);
                nodes.push(qName);
            }
        }
        
        @Override
        public void endElement(String uri, String localname, String qName) throws SAXException{
            if(nodes.size() > 0){
                if(nodes.peek().equalsIgnoreCase(qName)){
                    if(qName.equalsIgnoreCase("title")){
                        //stores title tag contents
                        output.put("title", accumulator);
                    }
                    else if(qName.equalsIgnoreCase("body")){
                        //stores body tag contents
                        output.put("content", accumulator);
                    }
                    else{
                        //close tag normally, did not consider auto-closing tag
                        accumulator += "</" + qName + ">";
                    }
                    //reached end of element, removes the stack
                    nodes.pop();
                }
                else if(nodes.peek().equalsIgnoreCase("embedded-content") && qName.equalsIgnoreCase("div")){
                    //it's a div, at closing embedded-content
                    //this code assumes only a SINGLE stage of package-entry - expect data loss on nesting package-entry
                    appendListToMap("embedded-content", accumulator);
                    oldAccumulatorGuard = false;
                    accumulator = oldAccumulator;
                    oldAccumulator = "";
                }
            }
        }
        
        @Override
        public void characters(char ch[], int start, int length) throws SAXException{
            if((nodes.size() > 0) && (nodes.contains("body") || nodes.peek().equalsIgnoreCase("title"))){
                //adds the line to accumulator
                accumulator += (new String(ch, start, length)).trim();
            }
        }
        
    }
    
    public MetaContainer parseTikaXML(String xml, String fileHash) throws DalianceException{
        try{
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            InternalXMLHandler handler = new InternalXMLHandler();
            parser.parse((new InputSource(new StringReader(xml))), handler);
            return PostParser.parse(handler.output, fileHash);
        }
        catch(IOException ex){
            throw(new DalianceException(getClass().getName() + ": Failed to read from string", ex));
        }
        catch(ParserConfigurationException ex){
            throw(new DalianceException(getClass().getName() + ": Parser initialization fails", ex));
        }
        catch(SAXException ex){
            throw(new DalianceException(getClass().getName() + ": XML parsing fails", ex));
        }
    }
    
}
