/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.outbound.tika;

import org.gamabox.daliance.config.ConfigProvider;
import org.gamabox.daliance.helper.Caster;
import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.logic.FilePackage;
import org.gamabox.daliance.logic.MetaContainer;
/**
 * ApacheTika middle layer
 * @author Tezla
 */
public class ApacheTika {
    private static ApacheTika instance;
    private final ApacheTikaClient clientInstance;
    private final int recusiveMetaThreshold;
    
    private ApacheTika() throws DalianceException{
        this.recusiveMetaThreshold = (new Integer(ConfigProvider.getInstance().getConfiguration("ApacheTika", "recusiveMetaThreshold")));
        this.clientInstance = new ApacheTikaClient();
    }
    
    /**
     * Creates ApacheTika instance.
     * @return
     * @throws DalianceException
     */
    public static ApacheTika getInstance() throws DalianceException {
        if(instance == null){
            instance = new ApacheTika();
        }
        return instance;
    }
    
    /**
     * Check availability of the service.
     * @throws DalianceException
     */
    private void checkAvailability() throws DalianceException {
        if(!this.clientInstance.isAvailable()){
            //if ApacheTika is deemed inactive, better throw some feedback.
            throw (new DalianceException(getClass().getName() + ": ApacheTika services is not available or not reachable."));
        }
    }
    
    /**
     * Get metadata from ApacheTika. Provided ApacheTika is up and running.
     * Metadata to be extracted either single or recursive one - extraction policy
     * defined in recusiveMetaThreshold entry in configuration.xml.
     * 
     * @param file
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException 
     */
    public MetaContainer getMetadata(FilePackage file) throws DalianceException{
        this.checkAvailability();
        
        if(file != null){
            //hides the real backend complication - it need to initiate new ParserXML as it preserves new states whenever it reads xml.
            MetaContainer ret = (new ParserXML()).parseTikaXML(this.clientInstance.getMeta(file.getPayload()).getContent(), file.getPayloadHash());
            //put base64 encoded binaries here, making sure it JSON-able safely and shorter than Hexadecimal.
            ret.setBinaries64(Caster.toBase64(file.getPayload()));
            return ret;
        }
        
        throw (new DalianceException(getClass().getName() + ": File payload can't be null!"));
    }
}
