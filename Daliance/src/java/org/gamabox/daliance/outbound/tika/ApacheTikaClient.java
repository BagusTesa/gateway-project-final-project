/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.tika;
import org.apache.http.client.fluent.Request;

import org.gamabox.daliance.config.ConfigProvider;
import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.outbound.RESTClient;
import org.gamabox.daliance.outbound.OutboundResponse;

/**
 * ApacheTikaClient should serve as bridge of this application and ApacheTika.
 * @author Tezla
 */
class ApacheTikaClient extends RESTClient{

    /**
     * Instantiate new ApacheTikaClient.
     * @throws DalianceException
     */
    public ApacheTikaClient() throws DalianceException{
        super(ConfigProvider.getInstance().getConfiguration("ApacheTika","url"),
              (new Integer(ConfigProvider.getInstance().getConfiguration("ApacheTika", "connectTimeout"))),
              (new Integer(ConfigProvider.getInstance().getConfiguration("ApacheTika", "socketTimeout"))));
    }
    
    /**
     * Dummy command, to check whether ApacheTika is up and running
     * @return OutboundResponse
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    @Override
    public OutboundResponse getHome() throws DalianceException{
        return Execute(ApplyConfig(Request.Get(this.url)));
    }
    
    /**
     * Get simple metadata and contents for a single file. All enclosed resources are not being extracted.
     * Returns OutboundResponse. Content in XML format.
     * @param fileBytes
     * @return OutboundResponse
     * @throws DalianceException
     */
    public OutboundResponse getMeta(byte[] fileBytes) throws DalianceException{
        return Execute(ApplyConfig(Request.Put(this.url + "tika").addHeader("Accept", "text/xml").bodyByteArray(fileBytes)));
    }
    
    /**
     * Get recursive metadata from ApacheTika. Thus all enclosed resources are being processed.
     * This method consumes more time and memory.
     * Returns OutboundResponse. Content in JSON format.
     * @param fileBytes
     * @return OutboundResponse
     * @throws DalianceException
     */
    public OutboundResponse getRMeta(byte[] fileBytes) throws DalianceException{
        return Execute(ApplyConfig(Request.Put(this.url + "rmeta").addHeader("Accept", "application/json").bodyByteArray(fileBytes)));
    }
    
}
