/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.outbound;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;

import org.gamabox.daliance.config.ConfigProvider;
import org.gamabox.daliance.helper.DalianceException;
/**
 * Rest Client abstract class. Handles aggregating response into a OutboundResponse class and common
 * configuration entries.
 * 
 * @author Tezla
 */
public abstract class RESTClient {
    
    /**
     * Stores User-Agent name to be reported to server.
     */
    protected final String userAgent;
    /**
     * URL to be called by Apache HTTP Component Fluent API.
     */
    protected final String url;

    /**
     * Apache HTTP Component Connection Timeout configuration.
     */
    protected final int connectionTimeout;

    /**
     * Apache HTTP Component Socket Timeout configuration.
     */
    protected final int socketTimeout;
    
    public RESTClient(String url, int connectionTimeout, int socketTimeout) throws DalianceException{
        this.url = url;
        this.connectionTimeout = connectionTimeout;
        this.socketTimeout = socketTimeout;
        this.userAgent = ConfigProvider.getInstance().getConfiguration("server", "userAgent");
    }
    
    /**
     * Append connection configurations to Request class in Apache HttpComponent
     * added configuration connection timeout and socket timeout.
     * 
     * @param in
     * @return Request
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    protected Request ApplyConfig(Request in) throws DalianceException{
        return in.connectTimeout((this.connectionTimeout)).socketTimeout(this.socketTimeout).userAgent(this.userAgent);
    };
    
    /**
     * Aggregates Apache HttpComponent response to OutboundResponse class
     * that contains HTTP Status Code and Response Content in form of String.
     * This function will return null if parameter in was null.
     * 
     * @param in
     * @return OutboundResonse
     * @throws org.gamabox.daliance.helper.DalianceException 
     */
    protected OutboundResponse Aggregator(Response in) throws DalianceException{
        if(in != null){
            try {
                //in could returnResponse() once - it is an input stream after all
                HttpResponse resp = in.returnResponse();
                
                InputStream instream = resp.getEntity().getContent();
                byte b[] = new byte[instream.available()];
                instream.read(b);
                
                return (new OutboundResponse(resp.getStatusLine().getStatusCode(), (new String(b))));
            } catch (IOException ex) {
                throw (new DalianceException(getClass().getName() + ": Unable to aggregate result", ex));
            }
        }
        return null;
    }
   
    /**
     * Execute the request and return OutboundResponse instance. This function is a shortcut
     * to call ApplyConfig followed by aggregator.
     * 
     * @param in
     * @return
     * @throws DalianceException
     */
    protected OutboundResponse Execute(Request in) throws DalianceException{
        try{
            return Aggregator(ApplyConfig(in).execute());
        }
        catch(IOException ex){
            throw (new DalianceException(getClass().getName() +  ": Unable to execute request", ex));
        }
    }
    
    /**
     * Internal method to get home of the target server.
     * 
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException*/
    public abstract OutboundResponse getHome() throws DalianceException;
    
    /**
     * Checks whether target server is active or not. Target server considered to be active
     * if it returns HTTP 200 (OK) Status Code with non-empty contents.
     * 
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    public boolean isAvailable() throws DalianceException{
        OutboundResponse resp = this.getHome();
        return (resp.getStatusCode() == 200) && (resp.getContent().length() > 0);
    }
    
}
