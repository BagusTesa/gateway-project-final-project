/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses.shards;

/**
 *
 * @author Tezla
 */
public class ShardsInformation {
    public int total;
    public int failed;
    public int successful;
}
