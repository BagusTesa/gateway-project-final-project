/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses;
import com.google.gson.annotations.SerializedName;

import org.gamabox.daliance.logic.MetaContainer;

/**
 * JSON POJO abstraction for get requests.
 * This POJO contains ItemBaseReponse fields with addition _source and found fields.
 * 
 * @author Tezla
 */
public class GetItemResponse extends ItemBaseResponse {
    /**
     * Stores whether object found or not for get and delete.
     * This field should be false if object not found or used from org.gamabox.daliance.outbound.response.search.Hits.
     */
    public boolean found;
    /**
     * Stores MetaContainer data in _source field if exists otherwise, null.
     */
    @SerializedName("_source") public MetaContainer data;
}
