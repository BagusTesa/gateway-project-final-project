/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses.mapping;
import java.util.List;
import java.util.LinkedList;

/**
 * Stores index's information, it's mapping including their fields.
 * 
 * @author Tezla
 */
public class MappingIndex implements Comparable<MappingIndex> {
    /**
     * Index name.
     */
    public String indexName;
    /**
     * Store mappings inside index.
     */
    public List<MappingItem> mappings;
    
    public List<String> getMappingsAsString(){
        if(this.mappings != null){
            List<String> out = new LinkedList<String>();
            for(MappingItem mappingIndices: this.mappings){
                out.add(mappingIndices.mappingName);
            }
            return out;
        }
        return null;
    }
    
    @Override
    public int compareTo(MappingIndex o) {
        if(o != null){
            return this.indexName.compareTo(o.indexName);
        }
        else{
            throw (new NullPointerException());
        }
    }
}
