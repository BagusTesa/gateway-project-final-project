/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;

import org.gamabox.daliance.config.ConfigProvider;
import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.outbound.OutboundResponse;
import org.gamabox.daliance.outbound.RESTClient;


/**
 * Handles Transaction with ElasticSearch
 * @author Tezla
 */
class ElasticSearchClient extends RESTClient{
    
    /**
     * Instantiate ElasticSearchClient.
     * @throws DalianceException
     */
    public ElasticSearchClient() throws DalianceException{
        super(ConfigProvider.getInstance().getConfiguration("ElasticSearch","url"),
              (new Integer(ConfigProvider.getInstance().getConfiguration("ElasticSearch", "connectTimeout"))),
              (new Integer(ConfigProvider.getInstance().getConfiguration("ElasticSearch", "socketTimeout"))));
    }

    @Override
    public OutboundResponse getHome() throws DalianceException {
        return Execute(ApplyConfig(Request.Get(this.url)));
    }
    
    /**
     * Get ElasticSearch type (mapping) for given index.
     * 
     * @param index
     * @return
     * @throws DalianceException
     */
    public OutboundResponse getMapping(String index) throws DalianceException {
        OutboundResponse in;
        if(index != null){
            //index-bound
            in = Execute(Request.Get(this.url + index + "/_mapping"));
        }
        else{
            //global mapping
            in = Execute(Request.Get(this.url + "_mapping"));
        }
        return in;
    }
    
    /**
     * Get ElasticSearch global Mapping.
     * 
     * @return
     * @throws DalianceException
     */
    public OutboundResponse getMapping() throws DalianceException {
        return this.getMapping(null);
    }
    
    /**
     * Create new document entry.
     * @param index
     * @param id
     * @param type
     * @param json
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    public OutboundResponse putDocument(String index, String type, String id, String json) throws DalianceException{
        OutboundResponse in;
        //url format url/index/type/{id} (PUT)
        in = Execute(Request.Put(this.url + index + "/" + type + "/" + id).bodyString(json, ContentType.APPLICATION_JSON));
        return in;
    }
    
    /**
     * Get document, specified by index, type, and id.
     * 
     * @param index
     * @param type
     * @param id
     * @return
     * @throws DalianceException
     */
    public OutboundResponse getDocument(String index, String type, String id) throws DalianceException {
        OutboundResponse in;
        //url format url/index/type/id (GET)
        in = Execute(Request.Get(this.url + index + "/" + type + "/" + id));
        return in;
    }
    
    /**
     * Search for document with given query. index and type is optional you can pass a null value (but not null string).
     * 
     * @param index
     * @param type
     * @param query
     * @return
     * @throws DalianceException
     */
    public OutboundResponse searchDocument(String index, String type, String query) throws DalianceException {
        OutboundResponse in;
        Request un;
        //url format url/{index}/{type}/{id} (GET)
        System.out.println(index + " " + type + " " + query);
        if((index != null) && (type != null)){
            un = Request.Post(this.url + index + "/" + type + "/_search");
        }
        else if(index != null){
            un = Request.Post(this.url + index + "/_search");
        }
        else{
            un = Request.Post(this.url + "_search");
        }
        in = Execute(un.bodyString(query, ContentType.APPLICATION_JSON));
        return in;
    }
    
    /**
     * Delete document based on index, type, and id. All parameters are mandatory.
     * 
     * @param index
     * @param type
     * @param id
     * @return
     * @throws DalianceException
     */
    public OutboundResponse deleteDocument(String index, String type, String id) throws DalianceException {
        //url format url/index/type/id (DELETE)
        return Execute(Request.Delete(this.url + index + "/" + type + "/" + id));
    }
    
    /**
     * Creates new index.
     * 
     * @param index
     * @return
     * @throws DalianceException
     */
    public OutboundResponse createIndex(String index) throws DalianceException {
        //url format url/index (POST)
        return Execute(Request.Post(this.url + index));
    }
    
    /**
     * Create index based on mapping configuration in json.
     * 
     * @param index
     * @param mappingParameters
     * @return
     * @throws DalianceException
     */
    public OutboundResponse createIndex(String index, String mappingParameters) throws DalianceException {
        //url format url/index (PUT)
        return Execute(Request.Put(this.url + index).bodyString(mappingParameters, ContentType.APPLICATION_JSON));
    }
    
    /**
     * Delete a mapping (type) from ElasticSearch.
     * Warning! This also removes all mapping and items that depends on this index.
     * 
     * @param index
     * @return
     * @throws DalianceException
     */
    public OutboundResponse deleteIndex(String index) throws DalianceException {
        //url format url/index (DELETE)
        return Execute(Request.Delete(this.url + index));
    }
    
}
