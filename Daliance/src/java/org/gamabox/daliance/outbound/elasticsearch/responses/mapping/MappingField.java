/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses.mapping;

import java.util.Map;

/**
 * Stores mapping's field information.
 * 
 * @author Tezla
 */
public class MappingField {
    /**
     * Store field's name.
     */
    public String fieldName;
    /**
     * Store field's configuration. This field only store if the configuration
     * is a JSON Primitives.
     */
    public Map<String, String> configuration;
}
