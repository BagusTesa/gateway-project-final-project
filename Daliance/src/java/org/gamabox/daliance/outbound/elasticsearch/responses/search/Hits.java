/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses.search;
import java.util.List;
import java.util.LinkedList;

import org.gamabox.daliance.logic.MetaContainer;


/**
 * POJO to simplify GSON's parsing.
 * This contains total number of search results, maximum score for given results, and
 * list of search result entries.
 * @author Tezla
 */
public class Hits {
    /**
     * Total search result, in this POJO we could verify with comparing this class hits field.
     */
    public int total;
    /**
     * Maximum (similarity) score in search.
     */
    public double max_score;
    /**
     * List of search items.
     */
    public List<SearchItemResponse> hits;
    
    /**
     *
     * @return
     */
    public List<MetaContainer> getSearchHitsAsMetaContainers(){
        List<MetaContainer> out = new LinkedList<MetaContainer>();
        if((hits != null) && (!hits.isEmpty())){
            for(SearchItemResponse cseek: hits){
                if(cseek.data != null){
                    out.add(cseek.data);
                }
            }
            return out;
        }
        return null;
    }
}
