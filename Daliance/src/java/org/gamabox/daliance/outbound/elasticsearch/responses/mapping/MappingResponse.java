/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses.mapping;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;

/**
 * A simplified mapping response POJO. This is loosely based on ElasticSearch
 * JSON.
 * 
 * @author Tezla
 */
public class MappingResponse {
    /**
     * Stores list of mapping.
     */
    public List<MappingIndex> list;
    
    /**
     * Get all indexes as string.
     * 
     * @return
     */
    public List<String> getIndexesAsString(){
        if(this.list != null){
            List<String> out = new LinkedList<String>();
            for(MappingIndex indexIndices: list){
                out.add(indexIndices.indexName);
            }
            return out;
        }
        return null;
    }
    
    /**
     * Search index by name.
     * 
     * @param indexName
     * @return
     */
    public MappingIndex getIndexByName(String indexName){
        if(this.list != null){
            MappingIndex ref = new MappingIndex();
            ref.indexName = indexName;
            int index = Collections.binarySearch(this.list, ref);
            if(index > -1){
                return this.list.get(index);
            }
        }
        return null;
    }
    
}
