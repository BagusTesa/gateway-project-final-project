/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.json;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.LinkedList;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.gamabox.daliance.helper.Caster;
import org.gamabox.daliance.outbound.elasticsearch.responses.mapping.MappingResponse;
import org.gamabox.daliance.outbound.elasticsearch.responses.mapping.MappingIndex;
import org.gamabox.daliance.outbound.elasticsearch.responses.mapping.MappingItem;
import org.gamabox.daliance.outbound.elasticsearch.responses.mapping.MappingField;

/**
 * A class solely to de-serialize the absurd response for mapping request.
 * 
 * @author Tezla
 */
public class MappingResponseDeserializer implements JsonDeserializer<MappingResponse>{

    @Override
    public MappingResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        MappingResponse out = new MappingResponse();
        MappingIndex _tmp;
        MappingItem _tmp2;
        MappingField _tmp3;
        
        out.list = new LinkedList<MappingIndex>();
        
        Set<Map.Entry<String, JsonElement>> indexEntry = json.getAsJsonObject().entrySet();
        //iterate for each indexes
        for(Entry<String, JsonElement> it: indexEntry){
            //fetch and register all indexes
            _tmp = new MappingIndex();
                out.list.add(_tmp);
            _tmp.indexName = it.getKey();
            //initiate space
            _tmp.mappings = new LinkedList<MappingItem>();
            //prepare to iterate over mappings
            JsonObject cseek = it.getValue().getAsJsonObject().get("mappings").getAsJsonObject();
            Set<Map.Entry<String, JsonElement>> mappingEntry = cseek.entrySet();
            //iterate for each mapping
            for(Entry<String, JsonElement> it2: mappingEntry){
                //fetch and register all mapping in indexes
                _tmp2 = new MappingItem();
                    _tmp.mappings.add(_tmp2);
                _tmp2.mappingName = it2.getKey();
                //initiate space
                _tmp2.fields = new LinkedList<MappingField>();
                //prepare to iterate over fields
                JsonObject cseek2 = it2.getValue().getAsJsonObject().get("properties").getAsJsonObject();
                Set<Map.Entry<String, JsonElement>> fieldEntry = cseek2.entrySet();
                //iterate all mapping parameters
                for(Entry<String, JsonElement> it3: fieldEntry){
                    _tmp3 = new MappingField();
                        _tmp2.fields.add(_tmp3);
                    _tmp3.fieldName = it3.getKey();
                    _tmp3.configuration = new HashMap<String, String>();
                    JsonObject cseek3 = it3.getValue().getAsJsonObject();
                    //prepare to iterate over field's configuration
                    Set<Map.Entry<String, JsonElement>> fieldConfig = cseek3.entrySet();
                    for(Entry<String, JsonElement> it4: fieldConfig){
                        if(it4.getValue().isJsonPrimitive()){
                            //assume all json primitives, then put into field configurations
                            _tmp3.configuration.put(it4.getKey(), it4.getValue().getAsString());
                        }
                    }
                }
            }
        }
        return out;
    }
}
