/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses;

/**
 * Stores acknowledged field, which used in creating or removing index.
 * 
 * @author Tezla
 */
public class GenericAcknowledgement {
    /**
     * ACK flag for creating or removing index.
     */
    public boolean acknowledged;
}
