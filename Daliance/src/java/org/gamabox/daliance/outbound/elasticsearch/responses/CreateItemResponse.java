/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses;

import com.google.gson.annotations.SerializedName;
import org.gamabox.daliance.outbound.elasticsearch.responses.shards.ShardsInformation;

/**
 * Response container for creating and updating item in ElasticSearch
 * (POST or PUT) items via url localhost:9200/index/type/{id}.
 * @author Tezla
 */
public class CreateItemResponse extends ItemBaseResponse{
    /**
     * Stores shard information if any.
     */
    @SerializedName("_shards") public ShardsInformation shards;
    /**
     * Flag whether new item created in index/mapping for put requests
     * If you update existing item, this flag will be false.
     */
    public boolean created;
}
