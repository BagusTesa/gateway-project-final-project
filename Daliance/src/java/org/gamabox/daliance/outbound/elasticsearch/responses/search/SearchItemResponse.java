/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses.search;
import com.google.gson.annotations.SerializedName;
import org.gamabox.daliance.outbound.elasticsearch.responses.GetItemResponse;

/**
 *
 * @author Tezla
 */
public class SearchItemResponse extends GetItemResponse{
    /**
     * Stores _score field which store Lucene's (ElasticSearch) similarity store for given document.
     * 
     */
    @SerializedName("_score") public double similarityScore;
}
