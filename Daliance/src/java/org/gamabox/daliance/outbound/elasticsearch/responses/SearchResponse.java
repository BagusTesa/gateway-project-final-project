/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses;
import com.google.gson.annotations.SerializedName;

import org.gamabox.daliance.outbound.elasticsearch.responses.search.Hits;
import org.gamabox.daliance.outbound.elasticsearch.responses.shards.ShardsInformation;

/**
 * Response container for search request, includes shards information.
 * 
 * @author Tezla
 */
public class SearchResponse {
    /**
     * Number of milliseconds to complete the search request.
     */
    public int took;
    /**
     * Flag whether any of ElasticSearch cluster get timed out.
     */
    public boolean timed_out;
    /**
     * Stores shard information if any.
     */
    @SerializedName("_shards") public ShardsInformation shards;
    public Hits hits;
}
