/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses;

import com.google.gson.annotations.SerializedName;

/**
 * POJO to store basic item information (index, mapping/type, id, and version), excluding _source.
 * 
 * @author Tezla
 */
public class ItemBaseResponse{
    /**
     * Stores index identifier.
     */
    @SerializedName("_index") public String index;
    /**
     * Stores mapping identifier.
     */
    @SerializedName("_type") public String type;
    /**
     * Store item id.
     */
    @SerializedName("_id") public String id;
    /**
     * Store item version.
     */
    @SerializedName("_version") public int version;
}
