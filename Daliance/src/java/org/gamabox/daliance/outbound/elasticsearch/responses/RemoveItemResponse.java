/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses;

import com.google.gson.annotations.SerializedName;
import org.gamabox.daliance.outbound.elasticsearch.responses.shards.ShardsInformation;

/**
 * POJO container for item removal request response.
 * @author Tezla
 */
public class RemoveItemResponse {
    /**
     * Stores shard information if any.
     */
    @SerializedName("_shards") public ShardsInformation shards;
    /**
     * Stores whether object found or not for get and delete.
     */
    public boolean found;
}
