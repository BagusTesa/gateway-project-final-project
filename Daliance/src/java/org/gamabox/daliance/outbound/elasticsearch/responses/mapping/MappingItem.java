/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch.responses.mapping;
import java.util.List;

/**
 * Stores mapping information, it's name and it's fields.
 * 
 * @author Tezla
 */
public class MappingItem implements Comparable<MappingItem> {
    /**
     * Mapping (type) name.
     */
    public String mappingName;
    /**
     * Mapping's fields.
     */
    public List<MappingField> fields;
    
    
    @Override
    public int compareTo(MappingItem o) {
        if(o != null)
            return this.mappingName.compareTo(o.mappingName);
        else
            throw (new NullPointerException());
    }
}
