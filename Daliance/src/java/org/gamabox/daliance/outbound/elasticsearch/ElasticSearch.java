/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.outbound.elasticsearch;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.gamabox.daliance.config.ConfigProvider;

import org.gamabox.daliance.helper.Caster;
import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.logic.MetaContainer;
import org.gamabox.daliance.outbound.OutboundResponse;
import org.gamabox.daliance.outbound.elasticsearch.json.MappingResponseDeserializer;
import org.gamabox.daliance.outbound.elasticsearch.responses.CreateItemResponse;
import org.gamabox.daliance.outbound.elasticsearch.responses.GenericAcknowledgement;
import org.gamabox.daliance.outbound.elasticsearch.responses.GetItemResponse;
import org.gamabox.daliance.outbound.elasticsearch.responses.RemoveItemResponse;
import org.gamabox.daliance.outbound.elasticsearch.responses.SearchResponse;
import org.gamabox.daliance.outbound.elasticsearch.responses.mapping.MappingResponse;


/**
 * ElasticSearch middle layer.
 * @author Tezla
 */
public class ElasticSearch{
    private static ElasticSearch instance;
    private final Gson gsonInstance;
    private final ElasticSearchClient clientInstance;
    private final String dataIndexName;
    
    private ElasticSearch() throws DalianceException{
        this.clientInstance = new ElasticSearchClient();
        this.gsonInstance = (new GsonBuilder()).registerTypeAdapter(MappingResponse.class, (new MappingResponseDeserializer())).create();
        this.dataIndexName = ConfigProvider.getInstance().getConfiguration("ElasticSearch", "dataIndexName");
    }
    
    /**
     * Get instance for ElasticSearch.
     * 
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    public static ElasticSearch getInstance() throws DalianceException{
        if(instance == null){
            instance = new ElasticSearch();
        }
        return instance;
    }
    
    private void checkAvailability() throws DalianceException {
        if(!this.clientInstance.isAvailable()){
            //if ElasticSearch is deemed inactive, better throw some feedback.
            //although the if clause above will surely throws exception
            throw (new DalianceException(getClass().getName() + ": ElasticSearch services is not available or not reachable."));
        }
    }
    
    /**
     * Get all mapping in Daliance index.
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException 
     */
    public List<String> getMapping() throws DalianceException{
        this.checkAvailability();
        OutboundResponse in = this.clientInstance.getMapping(this.dataIndexName);
        if(in.getStatusCode() == 200){
            MappingResponse mp = this.gsonInstance.fromJson(in.getContent(), MappingResponse.class);
            return mp.getIndexByName(this.dataIndexName).getMappingsAsString();
        }
        return null;
    }
    
    /**
     * Get ElasticSearch index for given mapping.
     * @param Mapping
     * @return
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    public List<MetaContainer> getAllDocuments(String Mapping) throws DalianceException{
        this.checkAvailability();
        OutboundResponse in = this.clientInstance.searchDocument(this.dataIndexName, Mapping, "{\"query\": {\"match_all\": {}}}");
        if(in.getStatusCode() == 200){
            //it's http = OK
            SearchResponse jresp = this.gsonInstance.fromJson(in.getContent(), SearchResponse.class);
            if(jresp != null){
                return jresp.hits.getSearchHitsAsMetaContainers();
            }
        }
        return null;
    }
    
    public List<MetaContainer> getAllDocuments() throws DalianceException {
        this.checkAvailability();
        OutboundResponse in = this.clientInstance.searchDocument(this.dataIndexName, null, "{\"query\": {\"match_all\": {}}}");
        if(in.getStatusCode() == 200){
            //it's http = OK
            SearchResponse jresp = this.gsonInstance.fromJson(in.getContent(), SearchResponse.class);
            if(jresp != null){
                return jresp.hits.getSearchHitsAsMetaContainers();
            }
        }
        return null;
    }
    
    /**
     * Get specific document by id.
     * @param id
     * @param type
     * @return
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    public MetaContainer getDocument(String id, String type) throws DalianceException{
        this.checkAvailability();
        OutboundResponse in = this.clientInstance.getDocument(this.dataIndexName, type, id);
        if(in.getStatusCode() == 200){
            //it's http = OK
            GetItemResponse jresp = this.gsonInstance.fromJson(in.getContent(), GetItemResponse.class);
            if((jresp.found == true) && (jresp.data != null)){
                //it's found, and parseable for MetaContainer
                return jresp.data;
            }
        }
        return null;
    }
    
    /**
     * Put a new document into ElasticSearch.
     * 
     * @param newDocument
     * @return 
     * @throws DalianceException
     */
    public boolean putDocument(MetaContainer newDocument) throws DalianceException{
        this.checkAvailability();
        OutboundResponse in = this.clientInstance.putDocument(this.dataIndexName, newDocument.getType(), newDocument.getUUID(), Caster.covJson(newDocument));
        if(in.getStatusCode() == 201){
            //it's http = Created
            CreateItemResponse jresp = this.gsonInstance.fromJson(in.getContent(), CreateItemResponse.class);
            //if it's created, then it created
            return jresp.created;
        }
        return false;
    }
    
    /**
     * Updates document.
     * 
     * @param updateDocument
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    public boolean updateDocument(MetaContainer updateDocument) throws DalianceException{
        this.checkAvailability();
        OutboundResponse in = this.clientInstance.putDocument(this.dataIndexName, updateDocument.getType(), updateDocument.getUUID(), Caster.covJson(updateDocument));
        if(in.getStatusCode() == 200){
            //it's http = OK
            CreateItemResponse jresp = this.gsonInstance.fromJson(in.getContent(), CreateItemResponse.class);
            //if created == true, you're creating new document! it should be false
            return !jresp.created;
        }
        else if(in.getStatusCode() == 201){
            //if new document created, it violates the agreement to insert and set type using putDocument.
            this.deleteDocument(updateDocument.getUUID(), updateDocument.getType());
        }
        return false;
    }
    
    /**
     * Delete document by MetaContainer instance.
     * 
     * @param document
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException
     */
    public boolean deleteDocument(MetaContainer document) throws DalianceException{
        return this.deleteDocument(document.getUUID(), document.getType());
    }
    
    public boolean deleteDocument(String id, String type) throws DalianceException{
        this.checkAvailability();
        OutboundResponse in = this.clientInstance.deleteDocument(this.dataIndexName, type, id);
        if(in.getStatusCode() == 200){
            //it's http = OK
            RemoveItemResponse jresp = this.gsonInstance.fromJson(in.getContent(), RemoveItemResponse.class);
            //if it's found, it's removed.
            return jresp.found;
        }
        return false;        
    }
    
    public boolean deleteIndex(String index) throws DalianceException{
        this.checkAvailability();
        OutboundResponse in = this.clientInstance.deleteIndex(index);
        if(in.getStatusCode() == 200){
            //it's http = OK
            GenericAcknowledgement jresp = this.gsonInstance.fromJson(in.getContent(), GenericAcknowledgement.class);
            return jresp.acknowledged;
        }
        return false;
    }
}
