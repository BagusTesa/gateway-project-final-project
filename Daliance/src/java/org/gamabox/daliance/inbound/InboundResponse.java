/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.inbound;

import java.util.List;
import java.io.Serializable;
import org.gamabox.daliance.helper.Caster;

/**
 * Makeshift for container for this servlet response.
 * This class will be serialized by GoogleJSON (GSON) Library
 *
 * @author Tezla
 */
public class InboundResponse implements Serializable{
    private int statusCode;
    private int payloadCount;
    private List<Object> payloads;
    
    /**
     * Generate JSON String for this class
     * @return 
     */
    public String toJSON(){
        //count number of payloads on serializing
        if(payloads != null){
            this.payloadCount = payloads.size();
        }
        //set GSON parameters, and returns string - so much for fluent style
        return Caster.covJson(this);
    }
    
    /**
     * Get this response statusCode.
     * @return
     */
    public int getStatusCode(){
        return this.statusCode;
    }
    
    /**
     * Set this response statusCode.
     * 
     * @param statusCode
     */
    public void setStatusCode(int statusCode){
        this.statusCode = statusCode;
    }
    
    /**
     * Get payload instance, so can be modified externally.
     * 
     * @return 
     */
    public List<Object> getPayloads(){
        return this.payloads;
    }
    
    /**
     * Set payload for this InboundResponse. Payload should be an instance of List of Objects
     * 
     * @param payloads
     */
    public void setPayloads(List<Object> payloads){
        this.payloads = payloads;
    }
    
}
