/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.inbound;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.logic.FilePackage;

/**
 * Servlet Base for Daliance Project.
 * 
 * @author Tezla
 */
public abstract class RESTServer extends HttpServlet{
    
    /**
     * Assemble CORS-able header response provided Origin Header is supplied.
     * This code also add Application/JSON header.
     * 
     * @param request
     * @param response
     */
    protected void makeHeader(HttpServletRequest request, HttpServletResponse response){
        if(request.getHeader("Origin") != null){
            //in case we can trace the origin
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        }
        response.setContentType("Application/JSON");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    }
    /**
     * Reads request binary data, assuming it is not a multi-part request.
     * 
     * @throws DalianceException
     * @param request
     * @return byte[]
     */
    protected FilePackage getFileAttachment(HttpServletRequest request) throws DalianceException{
        try{
            if((request.getContentType() != null) && !(request.getContentType().toLowerCase().contains("multipart/form-data"))){
                byte out[] = new byte[request.getContentLength()];
                InputStream in = request.getInputStream();
                DataInputStream chunker = new DataInputStream(in);
                chunker.readFully(out);
                chunker.close();
                return (new FilePackage(out));
            }
            return null;
        }
        catch(IOException ex){
            throw(new DalianceException(getClass().getName() + ": Unable to read bytestream from request", ex));
        }
    }
    
}
