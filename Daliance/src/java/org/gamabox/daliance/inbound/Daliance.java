/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.inbound;

import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.gamabox.daliance.helper.Caster;

import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.logic.MetaContainer;
import org.gamabox.daliance.outbound.tika.ApacheTika;
import org.gamabox.daliance.outbound.elasticsearch.ElasticSearch;
import org.gamabox.daliance.logic.FilePackage;

/**
 *
 * @author Tezla
 */
@WebServlet(name = "Daliance", urlPatterns = {"/*"})
public class Daliance extends RESTServer {
    
    /**
     * Get list of string for given URL Parameters.
     * ie: /Daliance/Something/SomeParameter returns list {Something, SomeParameter} while base url Daliance left.
     * This done using HttpServletRequest.getPathInfo(), and note that the URL is lower-cased.
     * 
     * @param request
     * @return
     */
    protected List<String> listURLParameter(HttpServletRequest request) {
        List<String> out = new LinkedList<String>();
        if(request.getPathInfo().compareTo("/") != 0){
            String tmp[] = request.getPathInfo().toLowerCase().split("/");
            out.addAll(Arrays.asList(tmp));
            //removes null string - "/Daliance/Something" being split with "/"
            out.remove("");
        }
        return out;
    }
    
    /**
     * Process application routing logic.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequestRouting(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //make default header
        this.makeHeader(request, response);
        
        List<String> url = this.listURLParameter(request);
        if(url.isEmpty()){ //- inherently url.size() == 0
            //it's /Daliance
            this.RouteRoot(request, response);
        }
        else{
            //Daliance/something
            if(url.get(0).compareTo("indexer") == 0){
                //indexer route group
                switch (url.size()) {
                    case 1:
                        //Indexer
                        this.RouteIndexer(request, response);
                        break;
                    case 2:
                        //Indexer/Item
                        if(url.get(1).compareTo("item") == 0){
                            this.RouteIndexerItem(request, response);
                        }
                        //Indexer/Type
                        else if(url.get(1).compareTo("type") == 0){
                            this.RouteIndexerType(request, response);
                        }
                        //
                        else{
                            this.Route404(request, response);
                        }
                        break;
                    case 3:
                        //Indexer/Type/{TypeName}
                        if(url.get(1).compareTo("type") == 0){
                            this.RouteIndexerTypeTypeName(request, response, url.get(2));
                        }
                        //
                        else{
                            this.Route404(request, response);
                        }
                        break;
                    case 4:
                        //Indexer/Type/{TypeName}/Item
                        if((url.get(1).compareTo("type") == 0) && (url.get(3).compareTo("item") == 0)){
                            this.RouteIndexerTypeTypeName(request, response, url.get(2));
                        }
                        else{
                            this.Route404(request, response);
                        }
                        break;
                    case 5:
                        //Indexer/Type/{TypeName}/Item/{UUID}
                        if((url.get(1).compareTo("type") == 0) && (url.get(3).compareTo("item") == 0)){
                            this.RouteIndexerTypeTypeNameItemUUID(request, response, url.get(2), url.get(4));
                        }
                        else{
                            this.Route404(request, response);
                        }
                        break;
                    case 6:
                        //Indexer/Type/{TypeName}/Item/{UUID}/Binary
                        if((url.get(1).compareTo("type") == 0) && (url.get(3).compareTo("item") == 0) && (url.get(5).compareTo("binary") == 0)){
                            this.RouteIndexerTypeTypeNameItemUUIDBinary(request, response, url.get(2), url.get(4));
                        }
                        else{
                            this.Route404(request, response);
                        }
                        break;
                    default:
                        this.Route404(request, response);
                        break;
                }
            }
            else if(url.get(0).compareTo("trainer") == 0){
                //trainer route group
                switch (url.size()) {
                    case 1:
                        //Trainer
                        break;
                    case 2:
                        //Trainer/Type
                        break;
                    case 3:
                        //Trainer/Type/{TypeName}
                        break;
                    case 4:
                        //Trainer/Type/{TypeName}/Item
                        break;
                    case 5:
                        //Trainer/Type/{TypeName}/Item/{UUID}
                        break;
                    case 6:
                        //Trainer/Type/{TypeName}/Item/{UUID}/Binary
                        break;
                    default:
                        break;
                }
            }
            else if(url.get(0).compareTo("searcher") == 0){
                //search request
            }
            else{
                
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    
    /**
     * Handles the HTTP <code>OPTIONS</code> method.
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doOptions(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequestRouting(request, response);
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequestRouting(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequestRouting(request, response);
    }
    
    /**
     *  Handles the HTTP <code>PUT</code> method.
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequestRouting(request, response);
    }
    
    /**
     * Handles the HTTP <code>DELETE</code> method.
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequestRouting(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Art thou mankind";
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Processor methods. Click on the + sign on the left to edit the code.">
    
    private void RouteMethodUnsupported(InboundResponse out, HttpServletRequest request, HttpServletResponse response){
        out.getPayloads().add("Method " + request.getMethod() + " did not supported, try HTTP OPTIONS first.");
        out.setStatusCode(404);
        response.setStatus(404);
    }
    
    private void Route404(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pr = response.getWriter();
        InboundResponse out = new InboundResponse();
            out.setPayloads(new LinkedList<Object>());
            out.getPayloads().add("Route " + request.getRequestURI() + "did not supported, consult the documentation first.");
        out.setStatusCode(404);
        response.setStatus(404);
        response.setHeader("Access-Control-Allow-Method", "");
    }
    
    private void RouteRoot(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pr = response.getWriter();
        response.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS");
        if(request.getMethod().compareToIgnoreCase("options") == 0){
            //do nothing, just answer with headers
        }
        else{
            InboundResponse out = new InboundResponse();
            List<Object> out_payload = new LinkedList<Object>();
                out.setPayloads(out_payload);
            out_payload.add("Read the docs");
            out.setStatusCode(200);
            pr.println(out.toJSON());
        }
        pr.close();
    }
    
    private void RouteIndexer(HttpServletRequest request, HttpServletResponse response) throws IOException{
        this.RouteRoot(request, response);
    }
    
    private void RouteIndexerItem(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pr = response.getWriter();
        InboundResponse out = new InboundResponse();
        List<Object> lobj = new LinkedList<Object>();
            out.setPayloads(lobj);
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        if(request.getMethod().compareToIgnoreCase("options") == 0){
            //do nothing, just answer with headers
        }
        else if(request.getMethod().compareToIgnoreCase("get") == 0) {
            try {
                List<MetaContainer> ls = ElasticSearch.getInstance().getAllDocuments();
                if(ls != null){
                    lobj.addAll(ls);
                }
                out.setStatusCode(200);
                response.setStatus(200);
            } catch (DalianceException ex) {
                Logger.getLogger(Daliance.class.getName()).log(Level.SEVERE, null, ex);
                
                lobj.add(ex);
                out.setStatusCode(500);
                response.setStatus(500);
            }
            
        }
        else if(request.getMethod().compareToIgnoreCase("post") == 0){
            try {
                FilePackage dataIn = this.getFileAttachment(request);
                
                MetaContainer meta = ApacheTika.getInstance().getMetadata(dataIn);
                //<stub> <!- change it later with proper categorization -!>
                meta.setType("document");
                //</stub>
                
                boolean isInsert = ElasticSearch.getInstance().putDocument(meta);
                
                if(isInsert){
                    lobj.add(meta);
                    out.setStatusCode(201);
                    response.setStatus(201);
                }
                else{
                    lobj.add("Failed to insert for unknown cause. It is possible the given UUID is already used.\n Note: The data however replacing the old one.");
                    out.setStatusCode(500);
                    response.setStatus(500);
                }
            } catch (DalianceException ex) {
                Logger.getLogger(Daliance.class.getName()).log(Level.SEVERE, null, ex);
                
                lobj.add(ex);
                out.setStatusCode(500);
                response.setStatus(500);
            }
        }
        else{
            this.RouteMethodUnsupported(out, request, response);
        }
        pr.println(out.toJSON());
        pr.close();
    }
    
    private void RouteIndexerType(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter pr = response.getWriter();
        InboundResponse out = new InboundResponse();
        List<Object> lobj = new LinkedList<Object>();
            out.setPayloads(lobj);
        response.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS");
        if(request.getMethod().compareToIgnoreCase("options") == 0){
            //answer with header only
        }
        else if(request.getMethod().compareToIgnoreCase("get") == 0){
            try {
                List<String> tmp = ElasticSearch.getInstance().getMapping();
                
                if(tmp != null){
                    lobj.addAll(tmp);
                }
                
                out.setStatusCode(200);
                response.setStatus(200);
            } catch (DalianceException ex) {
                Logger.getLogger(Daliance.class.getName()).log(Level.SEVERE, null, ex);
                
                lobj.add(ex);
                out.setStatusCode(500);
                response.setStatus(500);
            }
        }
        else{
            this.RouteMethodUnsupported(out, request, response);
        }
        pr.println(out.toJSON());
        pr.close();
    }
    
    private void RouteIndexerTypeTypeName(HttpServletRequest request, HttpServletResponse response, String TypeName) throws IOException {
        PrintWriter pr = response.getWriter();
        InboundResponse out = new InboundResponse();
        List<Object> lobj = new LinkedList<Object>();
            out.setPayloads(lobj);
        response.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS");
        if(request.getMethod().compareToIgnoreCase("options") == 0){
            //answer with header only
        }
        else if(request.getMethod().compareToIgnoreCase("get") == 0){
            try {
                List<MetaContainer> allDocuments = ElasticSearch.getInstance().getAllDocuments(TypeName);
                
                if(allDocuments != null){
                    lobj.addAll(allDocuments);
                }
                
                out.setStatusCode(200);
                response.setStatus(200);
            } catch (DalianceException ex) {
                Logger.getLogger(Daliance.class.getName()).log(Level.SEVERE, null, ex);
                
                lobj.add(ex);
                out.setStatusCode(500);
                response.setStatus(500);
            }
        }
        else{
            this.RouteMethodUnsupported(out, request, response);
        }
        pr.println(out.toJSON());
        pr.close();
    }
    
    private void RouteIndexerTypeTypeNameItemUUID(HttpServletRequest request, HttpServletResponse response, String TypeName, String UUID) throws IOException {
        PrintWriter pr = response.getWriter();
        InboundResponse out = new InboundResponse();
        List<Object> lobj = new LinkedList<Object>();
            out.setPayloads(lobj);
        response.setHeader("Access-Control-Allow-Methods", "GET, PUT, DELETE, OPTIONS");
        if(request.getMethod().compareToIgnoreCase("options") == 0){
            //answer with headers only
        }
        else if(request.getMethod().compareToIgnoreCase("get") == 0){
            try {
                MetaContainer ret = ElasticSearch.getInstance().getDocument(UUID, TypeName);
                
                if(ret != null){
                    lobj.add(ret);
                }
                
                out.setStatusCode(200);
                response.setStatus(200);
            } catch (DalianceException ex) {
                Logger.getLogger(Daliance.class.getName()).log(Level.SEVERE, null, ex);
                
                lobj.add(ex);
                out.setStatusCode(500);
                response.setStatus(500);
            }
        }
        else if(request.getMethod().compareToIgnoreCase("put") == 0){
            try {
                FilePackage tmp = this.getFileAttachment(request);
                
                MetaContainer _tmp = ApacheTika.getInstance().getMetadata(tmp);
                
                _tmp.setType(TypeName);
                _tmp.setUUID(UUID);
                
                if(ElasticSearch.getInstance().updateDocument(_tmp)){
                    lobj.add("Item " + UUID + " successfully updated.");
                    out.setStatusCode(500);
                    response.setStatus(500);
                }
                else{
                    lobj.add("Item " + UUID + " failed to be updated.");
                    out.setStatusCode(500);
                    response.setStatus(500);
                }
            } catch (DalianceException ex) {
                Logger.getLogger(Daliance.class.getName()).log(Level.SEVERE, null, ex);
                lobj.add(ex);
                out.setStatusCode(500);
                response.setStatus(500);
            }
            
        }
        else if(request.getMethod().compareToIgnoreCase("delete") == 0){
            try {
                if(ElasticSearch.getInstance().deleteDocument(UUID, TypeName)){
                    lobj.add("Item " + UUID + " successfully removed.");
                    out.setStatusCode(200);
                    response.setStatus(200);
                }
                else{
                    lobj.add("Item " + UUID + " failed to be removed.");
                    out.setStatusCode(500);
                    response.setStatus(500);
                }
            } catch (DalianceException ex) {
                Logger.getLogger(Daliance.class.getName()).log(Level.SEVERE, null, ex);
                
                lobj.add(ex);
                out.setStatusCode(500);
                response.setStatus(500);
            }
        }
        else{
            this.RouteMethodUnsupported(out, request, response);
        }
        pr.println(out.toJSON());
        pr.close();
    }
    
    private void RouteIndexerTypeTypeNameItemUUIDBinary(HttpServletRequest request, HttpServletResponse response, String TypeName, String UUID) throws IOException {
        InboundResponse out = new InboundResponse();
        List<Object> lobj = new LinkedList<Object>();
                out.setPayloads(lobj);
        response.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS");
        if(request.getMethod().compareToIgnoreCase("options") == 0){
            //answer with header only
        }
        else if(request.getMethod().compareToIgnoreCase("get") == 0){
            try {
                OutputStream outS = response.getOutputStream();
                MetaContainer tmp = ElasticSearch.getInstance().getDocument(UUID, TypeName);
                
                if(tmp != null){
                    outS.write(Caster.fromBase64(tmp.getBinaries64()));
                    response.setStatus(200);
                    if(tmp.getContentType().length() < 1){
                        response.setContentType("Application/Octet-Stream");
                    }
                    else{
                        response.setContentType(tmp.getContentType());
                    }
                }
            } catch (DalianceException ex) {
                Logger.getLogger(Daliance.class.getName()).log(Level.SEVERE, null, ex);
                
                PrintWriter pr = response.getWriter();
                
                lobj.add(ex);
                out.setStatusCode(500);
                response.setStatus(500);
                
                pr.println(out.toJSON());
                pr.close();
            }
        }
        else{
            this.RouteMethodUnsupported(out, request, response);
            PrintWriter pr = response.getWriter();
            pr.println(out.toJSON());
            pr.close();
        }
    }
    // </editor-fold>
}
