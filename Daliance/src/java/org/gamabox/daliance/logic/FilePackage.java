/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.logic;
import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.helper.HashProvider;

/**
 * Used to store byte[] and it's hash value.
 *
 * @author Tezla
 */
public class FilePackage {
    private final byte[] payload;
    private final String payloadHash;
    
    public FilePackage(byte[] payload) throws DalianceException{
        this.payload = payload;
        this.payloadHash = HashProvider.getInstance().Hash(payload);
    }
    
    public byte[] getPayload(){
        return this.payload;
    }
    
    public String getPayloadHash(){
        return this.payloadHash;
    }
    
}
