/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.logic.categorizer;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.gamabox.daliance.config.ConfigProvider;
import org.gamabox.daliance.helper.Caster;
import org.gamabox.daliance.helper.DalianceException;
import org.gamabox.daliance.logic.MetaContainer;

/**
 * Class used for calculating JaccardDistance of objects
 * @author Tezla
 */
public class JaccardProvider {
    private static JaccardProvider instance;
    private final double ContentTypeWeight;
    
    private JaccardProvider() throws DalianceException{
        ContentTypeWeight = Caster.str2double(ConfigProvider.getInstance().getConfiguration("categorizer", "contentTypeWeight"));
    }
    
    public static JaccardProvider getInstance() throws DalianceException{
        if(instance == null){
            instance = new JaccardProvider();
        }
        return instance;
    }
    
    /**
     * Build a list of metadata name
     * 
     * @param in
     * @return
     * @throws DalianceException
     */
    private List<String> jaccardMetaFlattener(Map<String, Object> in, String prefix) throws DalianceException{
        List<String> out = new LinkedList<String>();
        Object _tmp;
        String _tmp2;
        
        prefix = prefix.toLowerCase();
        for(String key: in.keySet()){
            _tmp = in.get(key);
            if(_tmp != null){
                if(_tmp instanceof Map){
                //it's a nested map, we've to dig deeper
                out.addAll(jaccardMetaFlattener((Map<String, Object>) _tmp, key));
                }
                else{
                    //render it flattened (yes flatten, should i use different keyword?) keys
                    out.add(prefix + "." + key.toLowerCase());
                }
            }
        }
        
        return out;
    }
    
    /**
     * Calculate Jaccard Distance of this instance with another object.
     * Vanilla Jaccard Distance done to field GenericsMeta, DublinCore, and XMP
     * while different ContentType gives inherent score 0.5 for distance.
     * 
     * @param itemA
     * @param itemB
     * @return
     * @throws DalianceException
     */
    public double jaccardDistance(MetaContainer itemA, MetaContainer itemB) throws DalianceException {
        double out = 0;
        int sameMetaField = 0;
        int thisMetaField = 0;
        int compMetaField = 0;
        List<String> thisMetaFieldList = new LinkedList<String>();
        List<String> compMetaFieldList = new LinkedList<String>();
        
        //calculate thisMetaFieldCount
            //adds GenericMeta
            thisMetaFieldList.addAll(this.jaccardMetaFlattener(itemA.getGenericsMeta(), "genericsmeta"));
            //adds DublinCore
            thisMetaFieldList.addAll(this.jaccardMetaFlattener(itemA.getDublinCore(), "dublincore"));
            //adds XMP
            thisMetaFieldList.addAll(this.jaccardMetaFlattener(itemA.getXMP(), "xmp"));
            //count fields
            thisMetaField = thisMetaFieldList.size();
        //calculate compMetaFieldCount
            //adds GenericMeta
            compMetaFieldList.addAll(this.jaccardMetaFlattener(itemB.getGenericsMeta(), "genericsmeta"));
            //adds DublinCore
            compMetaFieldList.addAll(this.jaccardMetaFlattener(itemB.getDublinCore(), "dublincore"));
            //adds XMP
            compMetaFieldList.addAll(this.jaccardMetaFlattener(itemB.getXMP(), "xmp"));
            //count fields
            compMetaField = compMetaFieldList.size();
        //calculate sameMetaField
            for(String key: thisMetaFieldList){
                if(compMetaFieldList.contains(key)){
                    sameMetaField++;
                }
            }
        //calculate JaccardIndex
        out = sameMetaField/((thisMetaField + compMetaField) - sameMetaField);
        
        if(itemA.getContentType().compareToIgnoreCase(itemB.getContentType()) != 0){
            //different ContentType, inherent cost.
            out -= 0.5;
        }
        //distance = 1 - jaccard index.
        return (1 - out);
    }
}
