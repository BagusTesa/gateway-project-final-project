/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.logic;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;
import java.util.HashMap;

/**
 * A java POJO to represent metadata, modeled after DublinCore Standard but did not implement all
 * dcterms as it irrelevant for several cases, any extended dcterms stored in DublinCore field
 * @author Tezla
 */
public class MetaContainer implements Serializable{
    private String UUID;
    private String FileHash;
    private String ContentType;
    private String Type;
    private String Binaries64;
    private String Title;
    private Date Created = new Date();
    private Date Modified = new Date();
    private String Contents;
    private final List<String> TableofContent = new LinkedList<String>();
    private final Map<String, Object> GenericsMeta = new HashMap<String, Object>();
    private final Map<String, Object> DublinCore = new HashMap<String, Object>();
    private final Map<String, Object> XMP = new HashMap<String, Object>();
    /**
     * Used to identify whether this Meta is a training data set or not.
     */
    private boolean training;

    /**
     * Return UUID
     * UUID used for identification in HBase and ElasticSearch
     * @return the UUID
     */
    public String getUUID() {
        return UUID;
    }
    
    public void setUUID(String UUID){
        this.UUID = UUID;
    }
    
    /**
     * @return the FileHash
     */
    public String getFileHash() {
        return FileHash;
    }

    /**
     * @param FileHash the FileHash to set
     */
    public void setFileHash(String FileHash) {
        this.FileHash = FileHash;
    }
    
    /**
     * Returns File Format/Content Type of the file
     * @return the ContentType
     */
    public String getContentType() {
        return ContentType;
    }

    /**
     * Set File Format/Content Type of the file
     * @param FileFormat the ContentType to set
     */
    public void setContentType(String FileFormat) {
        this.ContentType = FileFormat;
    }

    /**
     * Returns title of the file
     * @return the Title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * Set title of the file
     * @param Title the Title to set
     */
    public void setTitle(String Title) {
        this.Title = Title;
    }

    /**
     * @return the Type
     */
    public String getType() {
        return Type;
    }

    /**
     * @param Type the Type to set
     */
    public void setType(String Type) {
        this.Type = Type;
    }

    /**
     * Get Date instance of created date
     * @return the Created
     */
    public Date getCreated() {
        return Created;
    }

    /**
     * Set Date instance of created date
     * @param Created the Created to set
     */
    public void setCreated(Date Created) {
        this.Created = Created;
    }

    /**
     * Get Date instance of modified date
     * @return the Modified
     */
    public Date getModified() {
        return Modified;
    }

    /**
     * Set Date instance of modified date
     * @param Modified the Modified to set
     */
    public void setModified(Date Modified) {
        this.Modified = Modified;
    }

    /**
     * Returns string content of the file.
     * ApacheTika stores this in <body></body>
     * @return the Contents
     */
    public String getContents() {
        return Contents;
    }

    /**
     * Set string content of the file
     * @param Contents the Contents to set
     */
    public void setContents(String Contents) {
        this.Contents = Contents;
    }

    /**
     * Table of content made of any ApacheTika Field <div class="embedded" id=FILENAME/>
     * within body.
     * @return the TableofContent
     */
    public List<String> getTableofContent() {
        return TableofContent;
    }

    /**
     * This Map<String, Object> should only store any metadata that doesn't have
     * xmp*:* or dcterm:*
     * @return the GenericsMeta
     */
    public Map<String, Object> getGenericsMeta() {
        return GenericsMeta;
    }

    /**
     * Map<String, Object> to store dcterm:*
     * @return the DublinCore
     */
    public Map<String, Object> getDublinCore() {
        return DublinCore;
    }

    /**
     * Map<String, Object> to store xmp*:*
     * @return the XMP
     */
    public Map<String, Object> getXMP() {
        return XMP;
    }
    
    @Override
    public String toString(){
        String out = "";
        
        out += "UUID: " + this.UUID + "\n";
        out += "FileHash: " + this.FileHash + "\n";
        out += "Content-Type: " + this.ContentType + "\n";
        out += "Type: " + this.Type + "\n";
        out += "Title: " + this.Title + "\n";
        out += "Created: " + this.Created.toString() + "\n";
        out += "Modified: " + this.Modified.toString() + "\n";
        out += "Contents: " + this.Contents + "\n";
        out += "TableOfContents: " + this.TableofContent.toString() + "\n";
        out += "GenericsMeta: " + this.GenericsMeta.toString() + "\n";
        out += "DublinCore: " + this.DublinCore.toString() + "\n";
        out += "XMP: " + this.XMP.toString() + "\n";
        
        return out;
    }

    /**
     * Get Base64 representation of data binaries.
     * 
     * @return the Binaries64
     */
    public String getBinaries64() {
        return Binaries64;
    }

    /**
     * Set's Base64 representation of data binaries.
     * 
     * @param Binaries64 the Binaries64 to set
     */
    public void setBinaries64(String Binaries64) {
        this.Binaries64 = Binaries64;
    }

    /**
     * Check whether this MetaContainer is for training or not.
     * @return the training
     */
    public boolean isTraining() {
        return training;
    }

    /**
     * Sets this data as training or not.
     * @param training the training to set
     */
    public void setTraining(boolean training) {
        this.training = training;
    }
    
}
