/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.logic.keywords;

import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.gamabox.daliance.helper.GUIDProvider;
import org.gamabox.daliance.helper.Caster;
import org.gamabox.daliance.logic.MetaContainer;

/**
 * Post parser handles the generation of final abstraction for metadata, it maps Map<String, Object> into
 * MetaContainer instance.
 * @author Tezla
 */
public class PostParser {
    
    private static String[] processKey(String key){
        if((key != null) && (key.length() > 0)){
            //generalize all space as -
            String spaced   = key.replace(' ', '-');
            //split by :, used by dc*, xmp*, tiff, etc
            String splitted[] = spaced.split(":");
            String out[] = new String[splitted.length];
            //put into output
            out[0] = splitted[0];
            //if it actually splitted into two or more
            if(splitted.length > 1){
                out[1] = "";
                for(int i = 1; i < splitted.length; i++){
                    //everything after the key considered as value
                    out[1] += splitted[i];
                }
            }
            //return output
            return out;
        }
        return null;
    }
    
    private static Date parseDate(String strDate){
        if((strDate != null) && (strDate.length() > 0)){
            //example input: 2015-10-21T03:28:00Z
            //need to remove the T and Z to be recognized by Date class
            String tmp     = strDate.replaceAll("T|Z", " ");
            DateFormat fmt =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss ");
            try{
                //make date instance
                Date out = fmt.parse(strDate);
                return out;
            }
            catch(java.text.ParseException e){
                //well, i catch a failing format - not worth the flag
                return null;
            }
        }
        return null;
    }
    
    /**
     * Create MetaContainer instance based on Map<String, Object> and FileHash.
     * @param inStream
     * @param FileHash
     * @return
     */
    public static MetaContainer parse(Map<String, Object> inStream, String FileHash){
        if(inStream != null){
            MetaContainer out = new MetaContainer();
            Set<String> inKeys = inStream.keySet();
            //temporary variable - to store splitted meta-key
            String k[];
            Map _mp;
            String _st;
            Date _d;
            List _ls;
            //set uuid for identification
            out.setUUID(GUIDProvider.getGUID());
            //set's file hash
            out.setFileHash(FileHash);
            for(String key: inKeys){
                k = processKey(key);
                if(k.length > 1){
                    if(k[0].toLowerCase().contains("dc")){
                        out.getDublinCore().put(k[1], inStream.get(key));
                    }
                    else if(k[0].toLowerCase().contains("xmp")){
                        out.getXMP().put(k[1], inStream.get(key));
                    }
                    else{
                        //if nothing above, put it as is (inherently put a map in it) - so, the data will be actually grouped
                        if(out.getGenericsMeta().containsKey(k[0])){
                            Caster.safelyCastToMap(out.getGenericsMeta().get(k[0])).put(k[1], inStream.get(key));
                        }
                        else{
                            _mp = new HashMap<String, Object>();
                            _mp.put(k[1], inStream.get(key));
                            out.getGenericsMeta().put(k[0], _mp);
                        }
                    }
                }
                else{
                    //created, created-date, creation
                    if(key.toLowerCase().contains("creati") || key.toLowerCase().contains("create")){
                        _d = parseDate(Caster.safelyCastToString(inStream.get(key)));
                        if(_d != null){
                            //if the current 'created' date is older
                            if(_d.compareTo(out.getCreated()) == -1){
                                //save as created date
                                out.setCreated(_d);
                            }
                        }
                        else{
                            //should go to the else part.. but we have no goto
                            out.getGenericsMeta().put(key, inStream.get(key));
                        }
                    }
                    //modified, modified-date
                    else if(key.toLowerCase().contains("modifi")){
                        _d = parseDate(Caster.safelyCastToString(inStream.get(key)));
                        if(_d != null){
                            //if the current 'modified' date is newer
                            if(_d.compareTo(out.getModified()) == 1){
                                //save as modified date
                                out.setModified(_d);
                            }
                        }
                        else{
                            //should go to the else part.. but we have no goto
                            out.getGenericsMeta().put(key, inStream.get(key));
                        }
                    }
                    else if(key.equalsIgnoreCase("Content-Type")){
                        out.setContentType(Caster.safelyCastToString(inStream.get(key)));
                    }
                    else if(key.equalsIgnoreCase("Title")){
                        out.setTitle(Caster.safelyCastToString(inStream.get(key)));
                    }
                    else if(key.equalsIgnoreCase("Content")){
                        out.setContents(Caster.safelyCastToString(inStream.get(key)));
                    }
                    else if(key.equalsIgnoreCase("embedded")){
                        _ls = Caster.safelyCastToList(inStream.get(key));
                        if(_ls != null){
                            out.getTableofContent().addAll(_ls);
                        }
                        //else discard
                    }
                    else{
                        //if nothing above, put it as is
                        out.getGenericsMeta().put(key, inStream.get(key));
                    }
                }
            }
            return out;
        }
        return null;
    }
    
}
