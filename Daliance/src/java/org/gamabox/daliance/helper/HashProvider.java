/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.helper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.gamabox.daliance.config.ConfigProvider;
/**
 *
 * @author Tezla
 */
public class HashProvider {
    private static HashProvider instance;
    private String password = "aman123";
    private String algorithm = "SHA-256";
    private MessageDigest digest;
    
    private HashProvider() throws DalianceException{
        this.password = ConfigProvider.getInstance().getConfiguration("hashProvider", "password");
        this.algorithm = ConfigProvider.getInstance().getConfiguration("hashProvider", "algorithm");
    }
    
    /**
     * Get Daliance's HashProvider instance. All password and algorithm stored in configuration.xml.
     * If by any chance configuration.xml can't be read. This will be used instead:
     * password=aman123
     * algorithm=SHA-256
     * 
     * @return 
     * @throws org.gamabox.daliance.helper.DalianceException 
     */
    public static synchronized HashProvider getInstance() throws DalianceException{
        if(instance == null){
            instance = new HashProvider();
        }
        return instance;
    }
    
    /**
     * Create Hash String from string.
     * 
     * @param Message
     * @return
     * @throws DalianceException
     */
    public String Hash(String Message) throws DalianceException{
        return this.Hash(Caster.toByteArray(Message));
    }
    
    /**
     * Create Hash String from byte array.
     * 
     * @param Message
     * @return 
     * @throws DalianceException
     */
    public String Hash(byte[] Message) throws DalianceException{
        try{
            //ensure digest is in 'empty' state
            if(digest != null){
                digest.reset();
            }
            //do the hash slinging slicer!
            digest = MessageDigest.getInstance(algorithm);
            //better to define charset to standardize everything - however, we should think of fallback options
            digest.digest(password.getBytes());
            digest.update(Message);
            return Caster.toHexString(digest.digest());
        }
        catch(NoSuchAlgorithmException ex){
            throw (new DalianceException(getClass().getName() + ": No Such Algorithm Exception", ex));
        }
    }
    
}
