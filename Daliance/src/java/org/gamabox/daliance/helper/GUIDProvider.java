/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.helper;
import java.util.UUID;
/**
 * ID Provider, serialized
 * @author Tezla
 */
public class GUIDProvider{
    public static synchronized String getGUID(){
        //better random, avoid hotspotting in HBase
        return UUID.randomUUID().toString();
    }
}
