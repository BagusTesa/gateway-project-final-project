/*
 * GAMABOX Project
 * Research Only  * 
 */
package org.gamabox.daliance.helper;

/**
 * Custom exception, this application expected to have tons of exception to be handled, try-catch-ing each exception
 * on the higher part of application is not wise
 * @author Tezla
 */
public class DalianceException extends Exception{
    private static final long serialVersionUID = 1993103163737808019L;
    public DalianceException(){
        //
    }
    public DalianceException(String message){
        super(message);
    }
    public DalianceException(Throwable cause){
        super(cause);
    }
    public DalianceException(String message, Throwable cause){
        super(message, cause);
    }
    public DalianceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
