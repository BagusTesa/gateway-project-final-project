/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gamabox.daliance.helper;

import java.util.List;
import java.util.Map;
import javax.xml.bind.DatatypeConverter;
import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import org.gamabox.daliance.config.ConfigProvider;

/**
 * This class actually handles the casting trouble in Map<String, Object>
 * all function in this class should check whether its the instance of the target or not
 * if it possible to cast, it will return desired object instance - otherwise null.
 * @author Tezla
 */
public class Caster {
    
    private static Gson gsonInstance;
    
    /**
     * Cast input into String instance if possible, otherwise return null string instead.
     * 
     * @param in
     * @return
     */
    public static String safelyCastToString(Object in){
        if((in != null) && (in instanceof String)){
            return (String) in;
        }
        //returns null string instead of null pointer
        return "";
    }
    
    /**
     * Cast input into List "instance" if possible, otherwise return null.
     * 
     * @param in
     * @return
     */
    public static List safelyCastToList(Object in){
        if((in != null) && (in instanceof List)){
            return (List) in;
        }
        return null;
    }
    
    /**
     * Cast input Map "instance" if possible, otherwise return null.
     * 
     * @param in
     * @return 
     */
    public static Map safelyCastToMap(Object in){
        if((in != null) && (in instanceof Map)){
            return (Map) in;
        }
        return null;
    }
    
    /**
     * Convert byte array into a Hex-encoded String.
     * 
     * @param in
     * @return 
     */
    public static String toHexString(byte[] in){
        return DatatypeConverter.printHexBinary(in);
    }
    
    /**
     * Convert Hex-encoded String into byte array.
     * 
     * @param in
     * @return 
     */
    public static byte[] toByteArray(String in){
        return DatatypeConverter.parseHexBinary(in);
    }
    
    /**
     * Converts string to double via Double wrapper class parseDouble method.
     * 
     * @param in
     * @return
     */
    public static double str2double(String in){
        return (Double.parseDouble(in));
    }
    
    /**
     * Convert an array of bytes to Base64 representation.
     * 
     * @param in
     * @return 
     */
    public static String toBase64(byte in[]){
        return DatatypeConverter.printBase64Binary(in);
    }
    
    /**
     * Convert a Base64 String representation into array of bytes.
     * 
     * @param in
     * @return
     */
    public static byte[] fromBase64(String in){
        return DatatypeConverter.parseBase64Binary(in);
    }
    
    /**
     * Convert class into JSON using GSON.
     * configuration.xml should hold proper value for gson configuration.
     * 
     * @param obj
     * @return 
     */
    public static String covJson(Object obj){
        if(gsonInstance == null){
            //lazy load, make instance if it needed and store it (forever)
            GsonBuilder _tmp = new GsonBuilder();
            _tmp.serializeNulls().enableComplexMapKeySerialization();
            //print things in daliance
            _tmp.setPrettyPrinting();
            gsonInstance = _tmp.create();
        }
        return gsonInstance.toJson(obj);
    }
}
